package model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Column;

@Entity (name="Students3CSC")


public class Student {
	
	@Id

	private long Id;
	
	@Column (name="apelyido")
	private String lastName;
	
	@Column (name="pangalan")
	private String firstName;
	
	@Column (name="taonSaKolehiyo")
	private int yearLevel;
	
	@Column (name="kurso")
	private String course;
	
	
	public long getId() {
		return Id;
	}
	public void setId(long id) {
		this.Id = id;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public int getYearLevel() {
		return yearLevel;
	}
	public void setYearLevel(int yearLevel) {
		this.yearLevel = yearLevel;
	}
	public String getCourse() {
		return course;
	}
	public void setCourse(String course) {
		this.course = course;
	}
	
}
