<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    
    <%@ taglib prefix ="s" uri="/struts-tags" %>
    
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Payroll Salary Details</title>
</head>
<body>

	<h1><font color= "blue"> Using JSP Expression Language </font></h1>
	<h2>Hours Worked: ${hoursWorked}</h2>
	<h2>PayRate: Php ${payRate}</h2>
	<h2>GrossPay: ${grossPay}</h2>
	<hr/>
	<h1><font color= "green"> Using Struts 2 Tag Library </font></h1>
	<h2>Hours Worked: <s:property value="hoursWorked"/></h2>
	<h2>PayRate: Php <s:property value="payRate"/></h2>
	<h2>GrossPay: Php <s:property value="grossPay"/></h2>
	<form action="payrollform.jsp" method ="post">
		<input type = "submit" value = "Compute Another"/>
	</form>
	
	
	

</body>
</html>