package ust.payroll.action;

import ust.payroll.utility.ComputePayrollHelper;

public class ComputePayrollAction {

	private double hoursWorked;
	private double payRate;
	private double grossPay;
	

	public double getHoursWorked() {
		return hoursWorked;
	}
	
	public void setHoursWorked(double hoursWorked) {
		this.hoursWorked = hoursWorked;
		System.out.println("setHoursWorked executed");
	}

	public double getPayRate() {
		return payRate;
	}

	public void setPayRate(double payRate) {
		this.payRate = payRate;
		System.out.println("setPayRate executed");
	}

	public double getGrossPay() {
		return grossPay;
	}

	public void setGrossPay(double grossPay) {
		this.grossPay = grossPay;
	}


	public String execute(){
						
			ComputePayrollHelper payrollHelper = 
					new ComputePayrollHelper();
			
			setGrossPay(payrollHelper.computeGrossPay(hoursWorked, payRate));
		
					
			System.out.println("Hours Worked: " + getHoursWorked());
			System.out.println("Payrate: Php " + getPayRate());
			System.out.println("Gross Pay: Php: " + getGrossPay());	
				
			System.out.println("action class for payroll computation");
			return "success";
		
	}
	
}
