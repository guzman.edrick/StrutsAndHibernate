package ust.payroll.utility;

public class ComputePayrollHelper {

	public double computeGrossPay(double hoursWorked, double payRate){
	
		return (hoursWorked > 40) 
		? ( (hoursWorked * 40) + ((hoursWorked - 40) * 1.5 * payRate ))
		: (hoursWorked *40);
		
	}
	
}
