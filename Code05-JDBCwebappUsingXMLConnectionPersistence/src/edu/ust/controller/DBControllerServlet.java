package edu.ust.controller;

import java.beans.XMLDecoder;
import java.beans.XMLEncoder;
import java.io.IOException;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import java.sql.*;
import java.io.*;

import edu.ust.utility.Security;
import edu.ust.model.ConnectionBean;

@WebServlet("/index.html")
public class DBControllerServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	public Connection connection;
	//private String password;
	private ConnectionBean connBean;
	
	@PostConstruct
	public void readXMLDatabaseConfiguration() {
		//ConnectionBean connBean = new ConnectionBean();
		/*connBean.setUserName("prado-ics114-3csc");
		connBean.setPassword("nwVRt188kcHd0ioy5mCpLuLHpOGOl+HTiPvfzdMCChw=");
		connBean.setPort("1433");
		connBean.setDatabase("prado-ics114-3csc");
		connBean.setServer("127.0.0.1");
		connBean.setDriverName("net.sourceforge.jtds.jdbc.Driver");
		
		//create XML
		try {
			XMLEncoder encoder = new XMLEncoder(
					new BufferedOutputStream(
		            new FileOutputStream("C:\\ICS114/db_connection.xml")));
			encoder.writeObject(connBean);
			encoder.close();
		} catch (Exception e) {
		}*/
		
		try {
			XMLDecoder decoder = new XMLDecoder(
	                new BufferedInputStream(
	                    new FileInputStream("C:\\ICS114/db_connection.xml")));
			 connBean = (ConnectionBean) decoder.readObject();
			decoder.close();
		} catch (Exception e) {
		}	
	}
	
	public void init(ServletConfig config) throws ServletException {
		if (connBean == null) {
			System.err.println("connBean is NULL");
		}
		
		try {	
			Class.forName(connBean.getDriverName());
			String url = 
			  "jdbc:jtds:sqlserver://" + 
			  connBean.getServer() + ":" +connBean.getPort() + "/" + connBean.getDatabase(); 
			
			connection = 
			  DriverManager.getConnection(url,connBean.getUserName(),Security.decrypt(connBean.getPassword()));
		} catch (ClassNotFoundException cnfe) {
			System.err.println(cnfe.getMessage());
		} catch (SQLException sqle) {
			System.err.println(sqle.getMessage());
		}
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try {	
			if (connection != null) {
				Statement stmt = connection.createStatement();
				ResultSet rs = stmt.executeQuery("select * from employee");	
				System.out.println("Successful connection");
				//stmt.close();
				//connection.close();
				
				//perform binding to HttpSession
				HttpSession session = request.getSession();
				session.setAttribute("records", rs);
				
				RequestDispatcher dispatcher = 
					request.getRequestDispatcher("displayrecords.jsp");
				dispatcher.forward(request, response);
			} else {
				System.err.println("Connection is NULL.");
			}
		} catch (SQLException sqle){
			System.out.println("SQLException error occured - " 
				+ sqle.getMessage());
		}	
	}
}
