package edu.ust.utility;

import java.util.*;
import java.io.*;
import java.sql.*;

import edu.ust.model.ConnectionBean;


public class DBHelper {

	private static Connection connection;
	
	private DBHelper() {
		//need to restrict the instantiation of objects outside it
	}
	
	private static Connection getDBInstanceConnection() {
	try {	
		ConnectionBean connBean = new ConnectionBean();
		String propFile = "D:\\propfilejdbc\\propfile-3csc.property";
		FileInputStream fis = new FileInputStream(propFile);
		Properties property = new Properties();
		property.load(fis);
		
		connBean.setDatabase(property.getProperty("database"));
		connBean.setDriverName(property.getProperty("driverName"));
		connBean.setPassword(Security.decrypt(property.getProperty("password")));
		connBean.setPort(property.getProperty("port"));
		connBean.setUserName(property.getProperty("userName"));
		connBean.setPartURL(property.getProperty("jdbcPartURL"));
		connBean.setServer(property.getProperty("server"));
			
		//now instantiates the connection object
		
		
			Class.forName(connBean.getDriverName());
			String url = 
			  connBean.getPartURL()+"//" + 
			  connBean.getServer() + ":" 
			+connBean.getPort() + "/" + 
			  connBean.getDatabase(); 
			
			connection = 
			  DriverManager.getConnection(url,connBean.getUserName(),connBean.getPassword());
		} catch (ClassNotFoundException cnfe) {
			System.err.println(cnfe.getMessage());
		} catch (SQLException sqle) {
			System.err.println(sqle.getMessage());
		}
		catch (FileNotFoundException fnfe) {
			System.err.println(fnfe.getMessage());
		}
		catch (Exception e) {
			System.err.println(e.getMessage());
		}
		return connection;
}
		public static Connection getConnection(){
			return (connection!=null ?connection:getDBInstanceConnection());
}
}
