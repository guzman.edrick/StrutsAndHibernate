package edu.ust.controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import edu.ust.model.*;
import edu.ust.utility.DBHelper;
import edu.ust.utility.Security;

import java.util.*;
import java.io.*;
import java.sql.*;



@WebServlet("/getemployee.html")
public class GetEmployeesServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	private Connection connection;
	
	public void init(ServletConfig config) throws ServletException {
		super.init(config);
		connection = (Connection) getServletContext().getAttribute("connectionDAO");
	}

	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request, response);
	}

	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try {	
			if (connection != null) {
				Statement stmt = connection.createStatement();
				ResultSet rs = stmt.executeQuery("select * from employee order by lastname");	
				System.out.println("Successful connection");
				//stmt.close();
				//connection.close();
				
				//perform binding to HttpSession
				HttpSession session = request.getSession();
				session.setAttribute("records", rs);
				
				RequestDispatcher dispatcher = 
					request.getRequestDispatcher("displayrecords.jsp");
				dispatcher.forward(request, response);
			} else {
				System.err.println("Connection is NULL.");
			}
		} catch (SQLException sqle){
			System.out.println("SQLException error occured - " 
				+ sqle.getMessage());
		}	
	
	}
	

}
