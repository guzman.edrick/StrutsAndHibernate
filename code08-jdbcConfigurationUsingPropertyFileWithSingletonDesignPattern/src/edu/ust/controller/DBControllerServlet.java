package edu.ust.controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import edu.ust.model.*;
import edu.ust.utility.DBHelper;
import edu.ust.utility.Security;

import java.util.*;
import java.io.*;
import java.sql.*;


@WebServlet("/index.html")
public class DBControllerServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	
	private Connection connection;
//	Connection connection2;
	
	public void init(ServletConfig config) throws ServletException {
		super.init(config);
		connection = DBHelper.getConnection();
		getServletContext().setAttribute("connectionDAO", connection);
		
	/*	Connection x = DBHelper.getConnection();
		Connection y = DBHelper.getConnection();
		Connection z = DBHelper.getConnection();
		
		//test for reflexive property
		if(x.equals(x)){
			System.out.println("passed the reflexive property: x.equals(x)");
		}
		else{
			System.out.println("DID NOT pass the reflexive property: x.equals(x)");
		}

		//test for commutative property
		if(x.equals(y) && y.equals(x))
		{
			System.out.println("passed the symmetric property: " + "x.equals(y) and y.equals(x)");
		}
		else
		{
			System.out.println("DID NOT pass the symmetric property: " + "x.equals(y) and y.equals(x)");
		}
		
		//test for transitive property
				if(x.equals(y) && y.equals(x) && x.equals(z))
				{
					System.out.println("passed the transitive property: " + "x.equals(y) and y.equals(x) and x.equals(z)");
				}
				else
				{
					System.out.println("DID NOT pass the symmetric property: " + "passed the transitive property: " + "x.equals(y) and y.equals(x) and x.equals(z)");
				}
	*/
	}

	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request,response);
	}

	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try {	
			if (connection != null) {
				Statement stmt = connection.createStatement();
				ResultSet rs = stmt.executeQuery("select * from employee");	
				System.out.println("Successful connection");
				//stmt.close();
				//connection.close();
				
				//perform binding to HttpSession
				HttpSession session = request.getSession();
				session.setAttribute("records", rs);
				
				RequestDispatcher dispatcher = 
					request.getRequestDispatcher("displayrecords.jsp");
				dispatcher.forward(request, response);
			} else {
				System.err.println("Connection is NULL.");
			}
		} catch (SQLException sqle){
			System.out.println("SQLException error occured - " 
				+ sqle.getMessage());
		}	
	
	}

}
