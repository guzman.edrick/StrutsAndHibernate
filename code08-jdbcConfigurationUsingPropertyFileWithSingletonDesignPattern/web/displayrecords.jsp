<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
 
<jsp:useBean id="records" type="java.sql.ResultSet" 
	scope="session"/>    
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>JDBC Records</title>
</head>
<body>
	<h1>List of Records</h1>
	
	<table border="1">
		<tr>
			<th>ID</th>
			<th>Last Name</th>
			<th>First Name</th>
			<th>Position</th>
			<th>Department</th>
		</tr>
		<%
			while (records.next()) {
		%>
			<tr>
				<td><%=records.getInt("id") %></td>
				<td><%=records.getString("lastName") %></td>
				<td><%=records.getString("firstName") %></td>
				<td><%=records.getString("position") %></td>
				<td><%=records.getString("department") %></td>
			</tr>			
		<%  }%>
	</table>

	<p> Click <a href="getemployee.html">here</a> to verify records </p>

</body>
</html>