<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>

<jsp:useBean id="recordEmployees" type="java.sql.ResultSet" scope="request"/>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>UST 3CSC Employee Listing</title>
</head>
<body>
	<h1>Employee Listing</h1>
	
	<table border="1" width="100%" cellpadding="3" cellspacing="3">
		<tr>
			<th align="center">ID</th>
			<th align="center">Last Name</th>
			<th align="center">First Name</th>
			<th align="center">Position</th>
			<th align="center">Department</th>
			<th align="center">Action-Edit</th>
			<th align="center">Action-Delete</th>
		</tr>
		
			<% 
				while(recordEmployees.next()) {	
			%>
				<tr>
					<td><%=recordEmployees.getString("id")%></td>
					<td><%=recordEmployees.getString("lastname")%></td>
					<td><%=recordEmployees.getString("firstname")%></td>
					<td><%=recordEmployees.getString("position")%></td>
					<td><%=recordEmployees.getString("department")%></td>
					<td align="center">
					  <a href="employeemaintenance.html?id=<%=recordEmployees.getString("id")%>&action=edit">
					  	<img src="images/Editor.ico"/>
					  </a>
					</td>
					<td align="center">
					  <a href="employeemaintenance.html?id=<%=recordEmployees.getString("id")%>&action=delete">
					  	<img src="images/busy.ico"/>
					  </a>
					</td>
				</tr>			
	<% } %>
		
	</table>
</body>
</html>