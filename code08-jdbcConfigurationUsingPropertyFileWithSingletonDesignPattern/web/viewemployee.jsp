<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>3CSC Updated Record</title>
</head>
<body>
	<h1>3CSC Employee Updated Record Saved</h1>
	
	<%
		if(request.getParameter("success").equals("true")){ %>
			<h2 align="center">Successful record update.</h2>
			<p align="center">Last Name: ${employee.lastName}</p>
			<p align="center">First Name: ${employee.firstName}</p>
			<p align="center">Position: ${employee.position}</p>
			<p align="center">Department: ${employee.department}</p>
	<% } else { %>
			<h1>Employee record update failed</h1>
	<% } %>
</body>
</html>