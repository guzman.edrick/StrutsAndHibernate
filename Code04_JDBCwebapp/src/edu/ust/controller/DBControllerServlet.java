package edu.ust.controller;

import java.io.IOException;






import javax.annotation.PostConstruct;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import java.sql.*;

import edu.ust.utility.Security;

@WebServlet("/index.html")
public class DBControllerServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	public Connection connection;
	private String password;
	
/*	@PostConstruct
	public void doPasswordSecurity()
	{
		password = "prado-ics114-3csc";
		
		password = Security.encrypt("prado-ics114-3csc");
		System.out.println("Password encrypted value = " + password);
		
	}
	*/
	
	public void init(ServletConfig config) throws ServletException {
		try
		{
			Class.forName("net.sourceforge.jtds.jdbc.Driver");
			String username = "prado-ics114-3csc";
			String password = "nwVRt188kcHd0ioy5mCpLuLHpOGOl+HTiPvfzdMCChw=";
			String url = "jdbc:jtds:sqlserver://localhost:1433/prado-ics114-3csc";
			
			connection = DriverManager.getConnection(url, username, Security.decrypt(password));
		}	catch (ClassNotFoundException cnfe) {
			System.err.println(cnfe.getMessage());
		}
		catch (SQLException sqle){
			System.err.println(sqle.getMessage());
		}
		
	}

	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request, response);
	}

	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try{
			if(connection != null){
				Statement stmt = connection.createStatement();
				ResultSet rs = stmt.executeQuery("select * from Employee");
				System.out.println("Successful Connection");
				
				//stmt.close();
				//connection.close();
				
				//perform binding to HttpSession
				HttpSession session = request.getSession();
				session.setAttribute("records", rs);
				
				RequestDispatcher dispatcher =
						request.getRequestDispatcher("displayrecords.jsp");
				dispatcher.forward(request, response);				
			}
			else
			{
				System.err.println("Connection is NULL.");
			}
		}
		catch (SQLException sqle){
			System.err.println("SQLException error occured");
		}
	}

}
