<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Payroll Salary Details</title>
</head>
<body>

	<h1><font color= "blue"> Using JSP Expression Language </font></h1>
	<h2>Hours Worked: ${hoursWorked}</h2>
	<h2>PayRate: Php ${payRate}</h2>
	<h2>GrossPay: ${grossPay}</h2>
	<hr/>

</body>
</html>