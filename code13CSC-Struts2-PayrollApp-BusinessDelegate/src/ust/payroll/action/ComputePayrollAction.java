package ust.payroll.action;

import ust.payroll.utility.ComputePayrollHelper;

public class ComputePayrollAction {

	
		public String execute(){
			double hoursWorked = 65;
			double payRate = 500;
			
			ComputePayrollHelper payrollHelper = new ComputePayrollHelper();
			
			double grossPay = payrollHelper.computeGrossPay(hoursWorked, payRate);
					
			System.out.println("Hours Worked: " + hoursWorked);
			System.out.println("Payrate: Php " + payRate);
			System.out.println("Gross Pay: Php: " + grossPay);	
				
			System.out.println("action class for payroll computation");
			return "success";
		
	}
	
}
