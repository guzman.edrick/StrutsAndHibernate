package model;

import java.util.Date;

import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Column;
import javax.persistence.Lob;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;


@Entity (name="Students3CSCAutoID")


public class Student {
	
	@Id
	@GeneratedValue
	private long Id;
	
	@Column (name="apelyido")
	private String lastName;
	
	@Column (name="pangalan")
	private String firstName;
	
	@Column (name="taonSaKolehiyo")
	private int yearLevel;
	
	@Column (name="kurso")
	private String course;
	
	//additional fields
	@Temporal (TemporalType.TIMESTAMP)
	private Date dateEncoded;
	
	//@Lob
	@Transient
	private String description;
	
	
	
	
	public Date getDateEncoded() {
		return dateEncoded;
	}
	public void setDateEncoded(Date dateEncoded) {
		this.dateEncoded = dateEncoded;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public long getId() {
		return Id;
	}
	public void setId(long id) {
		this.Id = id;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public int getYearLevel() {
		return yearLevel;
	}
	public void setYearLevel(int yearLevel) {
		this.yearLevel = yearLevel;
	}
	public String getCourse() {
		return course;
	}
	public void setCourse(String course) {
		this.course = course;
	}
	
	@Embedded
	private Address address = new Address();




	public Address getAddress() {
		return address;
	}
	public void setAddress(Address address) {
		this.address = address;
	}
	
}
