package edu.ust.controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import edu.ust.model.*;
import edu.ust.utility.Security;

import java.util.*;
import java.io.*;
import java.sql.*;


@WebServlet("/index.html")
public class DBControllerServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	ConnectionBean connBean = new ConnectionBean();
	Connection connection;
	
	public void init(ServletConfig config) throws ServletException {
	
		try {	
		String propFile = "D:\\propfilejdbc\\propfile-3csc.property";
		FileInputStream fis = new FileInputStream(propFile);
		Properties property = new Properties();
		property.load(fis);
		
		connBean.setDatabase(property.getProperty("database"));
		connBean.setDriverName(property.getProperty("driverName"));
		connBean.setPassword(Security.decrypt(property.getProperty("password")));
		connBean.setPort(property.getProperty("port"));
		connBean.setUserName(property.getProperty("userName"));
		connBean.setPartURL(property.getProperty("jdbcPartURL"));
		connBean.setServer(property.getProperty("server"));
			
		//now instantiates the connection object
		
		
			Class.forName(connBean.getDriverName());
			String url = 
			  connBean.getPartURL()+"//" + 
			  connBean.getServer() + ":" 
			+connBean.getPort() + "/" + 
			  connBean.getDatabase(); 
			
			connection = 
			  DriverManager.getConnection(url,connBean.getUserName(),connBean.getPassword());
		} catch (ClassNotFoundException cnfe) {
			System.err.println(cnfe.getMessage());
		} catch (SQLException sqle) {
			System.err.println(sqle.getMessage());
		}
		catch (FileNotFoundException fnfe) {
			System.err.println(fnfe.getMessage());
		}
		catch (Exception e) {
			System.err.println(e.getMessage());
		}
	}

	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request,response);
	}

	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try {	
			if (connection != null) {
				Statement stmt = connection.createStatement();
				ResultSet rs = stmt.executeQuery("select * from employee");	
				System.out.println("Successful connection");
				//stmt.close();
				//connection.close();
				
				//perform binding to HttpSession
				HttpSession session = request.getSession();
				session.setAttribute("records", rs);
				
				RequestDispatcher dispatcher = 
					request.getRequestDispatcher("displayrecords.jsp");
				dispatcher.forward(request, response);
			} else {
				System.err.println("Connection is NULL.");
			}
		} catch (SQLException sqle){
			System.out.println("SQLException error occured - " 
				+ sqle.getMessage());
		}	
	
	}

}
