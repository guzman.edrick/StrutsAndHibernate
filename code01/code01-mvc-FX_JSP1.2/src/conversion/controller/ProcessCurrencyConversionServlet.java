package conversion.controller;

import java.io.IOException;


import javax.servlet.RequestDispatcher;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebInitParam;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import conversion.model.CurrencyBean;
import conversion.helper.Factory;


@WebServlet(
		description = "This Servlet perform currency conversion.", 
		urlPatterns = { "/processcurrencyconversion.html" }, 
		initParams = { 
				@WebInitParam(name = "usDollar", value = "46.6600", description = "US Currency against Philippine Peso"), 
				@WebInitParam(name = "euro", value = "52.8400", description = "Euro value"), 
				@WebInitParam(name = "ausDollar", value = "35.1500", description = "Australian Dollar"), 
				@WebInitParam(name = "japYen", value = "0.3816")
		})
public class ProcessCurrencyConversionServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
   
	double usDollar;
	double japYen;
	double euro;
	double ausDollar;
	
	public void init(ServletConfig config) throws ServletException {
		super.init(config);
		usDollar = Double.parseDouble(config.getInitParameter("usDollar"));
		euro = Double.parseDouble(config.getInitParameter("euro"));
		ausDollar = Double.parseDouble(config.getInitParameter("ausDollar"));
		japYen = Double.parseDouble(config.getInitParameter("japYen"));
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request, response);	
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		//data retrieval from the form
		double phpAmount = Double.parseDouble(request.getParameter("phpAmount"));
		String fxType  = request.getParameter("fxType");
		
		CurrencyBean currBean = Factory.getInstance(phpAmount, fxType);
		request.setAttribute("exchangeCurrency", currBean);
		
		RequestDispatcher dispatch = request.getRequestDispatcher("display.jsp");
		dispatch.forward(request, response);
	}

}
