package ust.faculty;

import java.sql.*;

public class TestDatabaseConnection {
	
	static Connection connection;

	public static void main(String[] args) {
		try
		{
			Class.forName("net.sourceforge.jtds.jdbc.Driver");
			String username = "prado-ics114-3csc";
			String password = "prado-ics114-3csc";
			String url = "jdbc:jtds:sqlserver://localhost:1433/prado-ics114-3csc";
			
			connection = DriverManager.getConnection(url, username, password);
			
			if(connection != null){
				Statement stmt = connection.createStatement();
				ResultSet rs = stmt.executeQuery("select * from Employee");
				System.out.println("Successful Connection");
				
				while (rs.next())
				{
					System.out.println(rs.getInt("id") + " , " +
							rs.getString("lastName") + " , " +
							rs.getString("firstName") + "  " + 
							rs.getString("position") + "   " +
							rs.getString("department"));
				}
				stmt.close();
				connection.close();
			
			
				
			}
			else
			{
				System.err.println("Connection is NULL.");
			}
		}
		catch (SQLException sqle){
			System.out.println("SQLException error occured - " + sqle.getMessage());
		}
		catch (ClassNotFoundException nfe)
		{
			System.out.println("ClassNotFoundException error occured - " + nfe.getMessage());
		}

	}

}
