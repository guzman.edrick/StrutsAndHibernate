package edu.ust.controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebInitParam;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import edu.ust.model.*;
import edu.ust.utility.Security;

import java.sql.*;



@WebServlet(
		urlPatterns = { "/index.html" }, 
		initParams = { 
				@WebInitParam(name = "database", value = "prado-ics114-3csc", description = "This is target database."), 
				@WebInitParam(name = "driverName", value = "net.sourceforge.jtds.jdbc.Driver", description = "The assigned JDBC JTDS driver class name."), 
				@WebInitParam(name = "jdbcPartURL", value = "jdbc:jtds:sqlserver:", description = "The assigned JDBC part URL."), 
				@WebInitParam(name = "port", value = "1433", description = "The assigned MS-SQL DB server port."), 
				@WebInitParam(name = "server", value = "127.0.0.1", description = "The IP address of the MS-SQL Server DB."), 
				@WebInitParam(name = "userName", value = "prado-ics114-3csc", description = "The assigned user id on the MS-SQL server DB."), 
				@WebInitParam(name = "password", value = "nwVRt188kcHd0ioy5mCpLuLHpOGOl+HTiPvfzdMCChw=", description = "The encrypted user password in the MS-SQL")
		})
public class DBControllerServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	ConnectionBean connBean = new ConnectionBean();
	Connection connection;
	
	public void init(ServletConfig config) throws ServletException {
		super.init(config);
		connBean.setDatabase(config.getInitParameter("database"));
		connBean.setDriverName(config.getInitParameter("driverName"));
		connBean.setPassword(config.getInitParameter("password"));
		connBean.setPort(config.getInitParameter("port"));
		connBean.setUserName(config.getInitParameter("userName"));
		connBean.setPartURL(config.getInitParameter("jdbcPartURL"));
		connBean.setServer(config.getInitParameter("server"));
			
		//now instantiates the connection object
		
		try {	
			Class.forName(connBean.getDriverName());
			String url = 
			  connBean.getPartURL()+"//" + 
			  connBean.getServer() + ":" 
			+connBean.getPort() + "/" + 
			  connBean.getDatabase(); 
			
			connection = 
			  DriverManager.getConnection(url,connBean.getUserName(),Security.decrypt(connBean.getPassword()));
		} catch (ClassNotFoundException cnfe) {
			System.err.println(cnfe.getMessage());
		} catch (SQLException sqle) {
			System.err.println(sqle.getMessage());
		}
		
		
	}

	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request,response);
	}

	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try {	
			if (connection != null) {
				Statement stmt = connection.createStatement();
				ResultSet rs = stmt.executeQuery("select * from employee");	
				System.out.println("Successful connection");
				//stmt.close();
				//connection.close();
				
				//perform binding to HttpSession
				HttpSession session = request.getSession();
				session.setAttribute("records", rs);
				
				RequestDispatcher dispatcher = 
					request.getRequestDispatcher("displayrecords.jsp");
				dispatcher.forward(request, response);
			} else {
				System.err.println("Connection is NULL.");
			}
		} catch (SQLException sqle){
			System.out.println("SQLException error occured - " 
				+ sqle.getMessage());
		}	
	}
	

}
