package edu.ust.erdbms.utility;

import edu.ust.erdbms.model.MRTBean;

public class BeanFactory {
	
	public static MRTBean getFactoryBean(String lastName, 
		String firstName, String position, 
		String department) {
		
		MRTBean employee = new MRTBean();
		employee.setLastName(lastName);
		employee.setFirstName(firstName);
		employee.setPosition(position);
		employee.setDepartment(department);
		return employee;
	}
}
