<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>3CSC Employee Record Update</title>
</head>
<body>
	<h1>3CSC Employee Record Update</h1>
	<form action="employeeupdate.html" method="post">
		<p>ID: ${param.id} </p>
		<p>LN: ${employeeRec.lastName} </p>
		<p>Last name: <input type="text" size="25" name="lastName" value="${employeeRec.lastName}"/></p>
		<p>First name: <input type="text" size="25" name="firstName" value="${employeeRec.firstName}"/></p>
		<p>Position: <input type="text" size="25" name="position" value="${employeeRec.position}"/></p>
		<p>Department: <input type="text" size="25" name="department" value="${employeeRec.department}"/></p>
		<input type="hidden" name="employeeId" value="${param.id}"/>
		<input type="submit" value="Update"/>
	</form>
	<center><p><a href="index.jsp">Click here to display the menu.</a></p></center>
</body>
</html>