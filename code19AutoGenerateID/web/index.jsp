<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Student Registration System</title>
</head>
<body>

<h1> Welcome to Student Registration System </h1>

<form action="processregistration.html" method = post>
	<fieldset>
		<legend> Student Details </legend>
		<p> ID: <input type = "number" name = "Id" /> </p>
		<p> Last Name: <input type = "text" name = "lastName" /> </p>
		<p> First Name: <input type = "text" name = "firstName" /> </p>
		<p> Year Level: <input type = "number" name = "yearLevel" /> </p>
		<p> Course: <input type = "text" name = "course" /> </p> 
		<p> Student Description 
			<textarea name = "description" rows ="5" cols="10" >
			Enter student description here.
			</textarea>
		</p> 
			
	</fieldset>
	
	<input type = "submit" value="Register"> &nbsp; &nbsp;
	<input type="reset">
	
	</form>

</body>
</html>